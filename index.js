console.log('JS DOM manipulation');

// [SECTION] Document Object Model (DOM)
    // allows us to access or modify the properties of an element in a webpage
    // its the standard on how to get, change, add or delete HTML elements
    // we'll be focusing only with DOM in terms of managing forms

    // For selecting HTML elements we will be using document.querySelector / getElementById
        // Syntax: document.querySelector("html element")

        // CSS selectors
            // class selector (.)
            // id selector (#)
            // tag selector (html tags)
            // universal selector(*)
            // attribute selector )[attribute]

        let universalSelector = document.querySelectorAll('*')

        let singleUniversalSelector = document.querySelector('*')
        console.log(universalSelector)
        console.log(singleUniversalSelector);

        let classSelector = document.querySelectorAll(".full-name")
        console.log(classSelector);

        let singleClassSelector = document.querySelector(".full-name")
        console.log(singleClassSelector);

        let idSelector = document.querySelector("#txt-first-name")
        console.log(idSelector);

        let tagSelector = document.querySelectorAll("input")
        console.log(tagSelector);

        let spanSelector = document.querySelector("span[id]")
        console.log(spanSelector);

        // getElement
            let element = document.getElementById('fullName')
            console.log(element);

            element = document.getElementsByClassName('full-name')
            console.log(element);

// [Section] Event Listeners
    // whenever a user interacts with a webpage, this action is considered as an event
    // Working with events is a large part of creating interactivity in a webpage
    // specifiic function that will be triggered if the event happen

    // the function us is "addEventListener", it takes two arguments
        // first argument a string identifying the event
        // second argument, function that the listener will trigger once the 'specified event' occur


        let txtFirstName = document.querySelector("#txt-first-name")
        // console.log(txtFirstName);

        // add event lsitener

        // keyup pag binitawan tska matriggerfunction
        // keydown matrigger agad pag ka click agad ng any
        txtFirstName.addEventListener('keyup', () => {
            // .value gagana lng sa mga forms. ung may input
            // .value kinukuha nya ung kung ano tinype mo sa form
            console.log(txtFirstName.value);
            spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
            // spanSelector.innerHTML = `<h1>${txtFirstName.value}</h1>>`
            
        })

        let txtLastName = document.querySelector("#txt-last-name")
        txtLastName.addEventListener('keyup', ()=>{
            spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}` 

        })

        let txtColor = document.querySelector("#text-color")
        txtColor.addEventListener('change',()=>{
            spanSelector.style.color = txtColor.value
            // spanSelector.className = txtColor.value

            console.log (txtColor.value)
    })


